// test binary integer literals

// Compiler versions know to work with this

// gcc-9
// clang-9

int main(int argc, char* argv[argc+1]) {
  return 0b1 + 0B0;
}
