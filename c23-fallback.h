#ifndef C23_FALLBACK_H
#define C23_FALLBACK_H

/**
 ** A header file to augment compatibility of older compilers with C23.
 **
 ** Commonly supported features that are required are
 ** - digit separators such as in 0xAB'CD
 ** - binary integer literals such as 0b1010 or 0B0101
 ** - the new attribute syntax such as [[deprectated]]
 **
 ** The first two are not easy to circumvent, so probably you should
 ** not even try to use C23 enabled code without having support for
 ** them.
 **
 ** For the attribute syntax there is a feature test
 ** __has_c_attribute. It can be used with define (or #ifdef) to test
 ** for the syntax itself and with #if and an argument to test for an
 ** individual attribute, see below.
 **
 ** Another feature test is __has_include, which similarly can be used
 ** to test for the preprocessor feature itself and if that is
 ** available to test for the availability of a specific header. This
 ** can be used to
 **
 ** - test for optional headers such as <complex.h>, <threads.h>, or
 **   <stdatomic.h>. This now allows to use the corresponding feature
 **   test macro (__STDC_NO_COMPLEX__, __STDC_NO_ATOMICS__,
 **   __STDC_NO_THREADS__) to test for compiler support of these
 **   features, separately from the availability of library
 **   interfaces.
 **
 ** - test for new headers that come with C23, namely <stdckdint.h>
 **   and <stdbit.h>
 **/

#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <limits.h>
// for bool, false and true
#include <stdbool.h>

// for static_assert
#include <assert.h>

// for call_once
#ifndef ONCE_FLAG_INIT
# ifdef __has_include0
#  if __has_include(<threads.h>)
#   warning "including call_once function from <threads.h> instead of <stdlib.h>"
#   include <threads.h>
#  endif
# endif
#endif

// If call_once has not been found we construct a function that is
// based on atomics.
#ifndef ONCE_FLAG_INIT
# ifdef __has_include
#  if __has_include(<stdatomic.h>)
#   include <stdatomic.h>
#   warning "emulating call_once function by using atomics"
enum { once_initial, once_inter, once_final, };
#   define ONCE_FLAG_INIT once_initial

typedef _Atomic(unsigned) once_flag;

# ifdef __has_c_attribute
#  if __has_c_attribute(__gnu__::__noinline__)
[[__gnu__::__noinline__]]
#  endif
# endif
static inline
void call_once(once_flag* flag, void callback(void)) {
  if (*flag != once_final) {
    unsigned expected = once_initial;
    if (atomic_compare_exchange_strong(flag, &expected, once_inter)) {
      callback();
      *flag = once_final;
    } else {
      while (*flag != once_final) {
        //
      }
    }
  } else {
    // We know already that the implementation does not support threads,
    // so the only available fence is this:
    atomic_signal_fence(memory_order_seq_cst);
  }
}
#  endif
# endif
#endif

// We assume that the compiler is already able to handle the new
// attribute syntax such as [[deprecated]]. Nevertheless, maybe
// certain attributes themselves might not yet be implemented. We
// provide fallback macros to corresponding gnu attributes. Here we
// use the "safe" names for these features with double underscores, so
// they don't interfere with application names.

#ifdef __has_c_attribute
# if !__has_c_attribute(__unsequenced__) && __has_c_attribute(__gnu__::__const__)
#  warning "emulating unsequenced attribute via gnu attribute"
#  define __unsequenced__ __gnu__::__const__
# endif

# if !__has_c_attribute(__reproducible__) && __has_c_attribute(__gnu__::__pure__)
#  warning "emulating reproducible attribute via gnu attribute"
#  define __reproducible__ __gnu__::__pure__
# endif

# if !__has_c_attribute(__deprecated__) && __has_c_attribute(__gnu__::__deprecated__)
#  warning "emulating deprecated attribute via gnu attribute"
#  define __deprecated__ __gnu__::__deprecated__
# endif

# if !__has_c_attribute(__nodiscard__) && __has_c_attribute(__gnu__::__warn_unused_result__)
#  warning "emulating nodiscard attribute via gnu attribute"
#  define __nodiscard__ __gnu__::__warn_unused_result__
# endif

# if !__has_c_attribute(__maybe_unused__) && __has_c_attribute(__gnu__::__unused__)
#  warning "emulating maybe_unused attribute via gnu attribute"
#  define __maybe_unused__ __gnu__::__unused__
# endif

# if !__has_c_attribute(__noreturn__) && __has_c_attribute(__gnu__::__noreturn__)
#  warning "emulating noreturn attribute via gnu attribute"
#  define __noreturn__ __gnu__::__noreturn__
# endif

# if !__has_c_attribute(__fallthrough__) && __has_c_attribute(__gnu__::__fallthrough__)
#  warning "emulating fallthrough attribute via gnu attribute"
#  define __fallthrough__ __gnu__::__fallthrough__
# endif

// Deprecate two interfaces in <time.h>
[[__deprecated__]] char *asctime(const struct tm*);
[[__deprecated__]] char *ctime(const time_t*);

#endif

/* C23 has three new tg interfaces in the new <stdkdint.h> header.
They are modeled after similar gcc features. They are meant to do
arithmetic with overflow check by using everything the compiler can
get from instruction flags that already exist on most CPU.
*/

#ifdef __has_include
# if __has_include(<stdckdint.h>)
#  include <stdckdint.h>
# endif
#endif

/* If we don't have the header, yet, we may easily emulate it if we
are on a compiler claiming compatibility with gcc. */
#ifndef ckd_add
# ifdef __GNUC__
#  define ckd_add(R, A, B) __builtin_add_overflow ((A), (B), (R))
#  define ckd_sub(R, A, B) __builtin_sub_overflow ((A), (B), (R))
#  define ckd_mul(R, A, B) __builtin_mul_overflow ((A), (B), (R))
# else
#  error "we need a compiler extension for this"
# endif
#endif

// C23 has nullptr, use a fallback if that is not available. The
// constant may suitably be emulated in C17, but the type nullptr_t
// *may not*. Type generic programming with null pointers needs more
// complicated machinery in C17.
#if __STC_VERSION__ < 202300L
# define nullptr ((void*)0)
#endif

// C23 has the unreachable macro in <stdlib.h>, use a fallback if that
// is not available
#ifndef unreachable
# define unreachable() __builtin_unreachable()
#endif

// C23 provides macros for the width of integer types. Provide a
// fallback if they are not available

#ifndef BOOL_WIDTH
# define BOOL_WIDTH 1
#endif

#ifndef CHAR_WIDTH
# define CHAR_WIDTH CHAR_BIT
#endif

#ifndef UCHAR_WIDTH
# define UCHAR_WIDTH CHAR_BIT
#endif

#ifndef SCHAR_WIDTH
# define SCHAR_WIDTH CHAR_BIT
#endif

#ifndef USHRT_WIDTH
# if USHRT_MAX <= 0xFFFFU
#  define USHRT_WIDTH 16
#  define SHRT_WIDTH 16
# else
#  if USHRT_MAX <= 0xFFFFFFFFU
#   define USHRT_WIDTH 32
#   define SHRT_WIDTH 32
#  else
#   if USHRT_MAX <= 0xFFFFFFFFFFFFFFFFU
#    define USHRT_WIDTH 64
#    define SHRT_WIDTH 64
#   else
// This doesn't use #elif but nested #if/#else because the value might
// be too large for the preprocessor
#    if USHRT_MAX <= 0xFFFFFFFFFFFFFFFFFFFFFFFFU
#     define USHRT_WIDTH 96
#     define SHRT_WIDTH 96
#    else
// This doesn't use #elif but nested #if/#else because the value might
// be too large for the preprocessor
#     if USHRT_MAX <= 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFU
#      define USHRT_WIDTH 128
#      define SHRT_WIDTH 128
#     endif
#    endif
#   endif
#  endif
# endif
#endif

// unsigned int has a width of at least 16.
#ifndef UINT_WIDTH
# if UINT_MAX <= 0xFFFFU
#  define UINT_WIDTH 16
#  define INT_WIDTH 16
# else
#  if UINT_MAX <= 0xFFFFFFFFU
#   define UINT_WIDTH 32
#   define INT_WIDTH 32
#  else
#   if UINT_MAX <= 0xFFFFFFFFFFFFFFFFU
#    define UINT_WIDTH 64
#    define INT_WIDTH 64
#   else
// This doesn't use #elif but nested #if/#else because the value might
// be too large for the preprocessor
#    if UINT_MAX <= 0xFFFFFFFFFFFFFFFFFFFFFFFFU
#     define UINT_WIDTH 96
#     define INT_WIDTH 96
#    else
// This doesn't use #elif but nested #if/#else because the value might
// be too large for the preprocessor
#     if UINT_MAX <= 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFU
#      define UINT_WIDTH 128
#      define INT_WIDTH 128
#     endif
#    endif
#   endif
#  endif
# endif
#endif

// unsigned long has a width of at least 32
#ifndef ULONG_WIDTH
# if ULONG_MAX <= 0xFFFFFFFFUL
#  define ULONG_WIDTH 32
#  define LONG_WIDTH 32
# else
#  if ULONG_MAX <= 0xFFFFFFFFFFFFFFFFUL
#   define ULONG_WIDTH 64
#   define LONG_WIDTH 64
#  else
// This doesn't use #elif but nested #if/#else because the value might
// be too large for the preprocessor
#   if ULONG_MAX <= 0xFFFFFFFFFFFFFFFFFFFFFFFFUL
#    define ULONG_WIDTH 96
#    define LONG_WIDTH 96
#   else
// This doesn't use #elif but nested #if/#else because the value might
// be too large for the preprocessor
#    if ULONG_MAX <= 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFUL
#     define ULONG_WIDTH 128
#     define LONG_WIDTH 128
#    endif
#   endif
#  endif
# endif
#endif

// unsigned long long has a width of at least 64.
#ifndef ULLONG_WIDTH
# if ULLONG_MAX <= 0xFFFFFFFFFFFFFFFFULL
#  define ULLONG_WIDTH 64
#  define LLONG_WIDTH 64
# else
// This doesn't use #elif but nested #if/#else because the value might
// be too large for the preprocessor
#  if ULLONG_MAX <= 0xFFFFFFFFFFFFFFFFFFFFFFFFULL
#   define ULLONG_WIDTH 96
#   define LLONG_WIDTH 96
#  else
// This doesn't use #elif but nested #if/#else because the value might
// be too large for the preprocessor
#   if ULLONG_MAX <= 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFULL
#    define ULLONG_WIDTH 128
#    define LLONG_WIDTH 128
#   endif
#  endif
# endif
#endif

// In most cases the [u]intmax_t types will just be the long long
// types.
#ifndef UINTMAX_WIDTH
# if UINTMAX_MAX == ULLONG_MAX
#  define UINTMAX_WIDTH ULLONG_WIDTH
#  define INTMAX_WIDTH  LLONG_WIDTH
# endif
#endif

// In C23 uint128_t may exist, even if it is wider than
// uintmax_t. Provide it if the compiler supports it.
#ifndef UINT128_MAX
# ifdef __SIZEOF_INT128__
typedef signed __int128 int128_t;
typedef unsigned __int128 uint128_t;
#  define UINT128_WIDTH 128
#  define INT128_WIDTH  128
#  define UINT128_MAX   ((uint128_t)-1)
#  define INT128_MAX    ((int128_t)(((uint128_t)-1)>>1))
#  define INT128_MIN    (-INT128_MAX-1)
# endif
#endif

// In C23 uint256_t may exist, even if it is wider than
// uintmax_t. Provide it if the compiler supports it.
#ifndef UINT256_MAX
# ifdef __SIZEOF_INT256__
typedef signed __int256 int256_t;
typedef unsigned __int256 uint256_t;
#  define UINT256_WIDTH 256
#  define INT256_WIDTH  256
#  define UINT256_MAX   ((uint256_t)-1)
#  define INT256_MAX    ((int256_t)(((uint256_t)-1)>>1))
#  define INT256_MIN    (-INT256_MAX-1)
# endif
#endif

// Complex arithmetic without <complex.h> header
#ifndef __STDC_NO_COMPLEX__

// gcc and related compilers implement an "I" or "i" suffix for
// floating point constants since ages.
# ifdef __GNUC__
#  define I (__extension__ 1.0IF)
#  define _COMPLEX_I (0.0F + I)

// Otherwise use the new constexpr construct to produce a named
// constant of complex type.
# else
typedef union _C23_complex _C23_complex;
union _C23_complex {
  // These two are guaranteed to have the same representation.
  float _C23_v[2];
  _Complex float _C23_c;
};
// Use static const. Best would be to have constexpr, but using a
// different member than was initialized is not defined for them,
// unfortunately.
static _C23_complex const _C23_I = { { 0.0F, 1.0F, }, };
#  define _COMPLEX_I (_C23_I._C23_c)
# endif

#define iscomplex(X) (sizeof(1.0F*(X)) == sizeof(1.0F*(X)+_COMPLEX_I))
#define isimaginary(X) (sizeof(1.0F*(X)+0.0F) == 2*sizeof(1.0F*(X)))

# define _C23_float_Complex_CASE float _Complex: true,
# define _C23_double_Complex_CASE double _Complex: true,
# define _C23_long_double_Complex_CASE long double _Complex: true,
#else
# define _C23_float_Complex_CASE
# define _C23_double_Complex_CASE
# define _C23_long_double_Complex_CASE
#endif

// Testing for presence of decimal floating types

#if defined(__DEC32_MAX__)
# define _C23_Decimal32_CASE _Decimal32: true,
#else
# define _C23_Decimal32_CASE
#endif

#if defined(__DEC64_MAX__)
# define _C23_Decimal64_CASE _Decimal64: true,
#else
# define _C23_Decimal64_CASE
#endif

#if defined(__DEC128_MAX__)
# define _C23_Decimal128_CASE _Decimal128: true,
#else
# define _C23_Decimal128_CASE
#endif

#define isdecimalfloating(X) (_Generic((X)+0, _C23_Decimal32_CASE _C23_Decimal64_CASE _C23_Decimal128_CASE default: false))
#ifndef iscomplex
# define iscomplex(X) (_Generic((X)+0, _C23_float_Complex_CASE _C23_double_Complex_CASE _C23_long_double_Complex_CASE default: false))
#endif
#define isstandardrealfloating(X) (_Generic((X)+0, float: true, double: true, long double: true, default: false))
#define isstandardfloating(X) ((bool)(isstandardrealfloating(X)||iscomplex(X)))
#define isfloating(X) ((bool)(isstandardfloating(X)||isdecimalfloating(X)))
#define isinteger(X) ((bool)!isfloating(X))

#if __STDC_VERSION__ < 202300L
# define totype(X, Y) ((__typeof__(X))Y)
#else
# define totype(X, Y) ((typeof(X))Y)
#endif
#define tominusone(X) totype(X, -1)
#define tozero(X) totype(X, 0)
#define issigned(X) ((tominusone(X) < tozero(X)) && isinteger(X) && _Generic((X), char: false, default: true))
#define isunsigned(X) ((tominusone(X) > tozero(X)) && isinteger(X) && _Generic((X), char: false, default: true))

#endif
