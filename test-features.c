// test for features

// Compiler versions know to work with this

// gcc-??
// clang-??

#include "c23-fallback.h"
#include <stdio.h>

#define test(...) printf("Testing " #__VA_ARGS__ ": %lld\n", (long long)__VA_ARGS__)

int main(int argc, char* argv[argc+1]) {
  test(iscomplex(0.0));
  test(isimaginary(0.0));
  test(isfloating(0.0));
  test(iscomplex(I));
  test(isimaginary(I));
  test(isfloating(I));
  test(iscomplex(0U));
  test(isimaginary(0U));
  test(isfloating(0U));
  test(isunsigned((char)'A'));
  test(issigned((char)'A'));
  test(isunsigned((unsigned char)'A'));
  test(issigned((unsigned char)'A'));
  test(tominusone((char)'A'));
  test(tominusone((unsigned char)'A'));
}
