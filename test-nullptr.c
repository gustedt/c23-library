// test for nullptr

// Compiler versions know to work with this

// gcc-13
// clang-??

int main(int argc, char* argv[argc+1]) {
  int* a = nullptr;
  if (!a) a = &argc;
  return *a;
}
