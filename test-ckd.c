#include <stdio.h>
#include <signal.h>
#if __has_include(<stdatomic.h>)
# include <stdatomic.h>
#endif

#include "c23-fallback.h"


typedef unsigned long ulong;
typedef unsigned long long ullong;
typedef signed long long llong;

#ifndef OVERFLOW_HANDLER
# if  defined(OVERFLOW_UB)
#  define OVERFLOW_HANDLER() unreachable()
# elif defined(OVERFLOW_FPE)
#  define OVERFLOW_HANDLER() raise(SIGFPE)
# elif defined(OVERFLOW_IGNORE)
#  define OVERFLOW_HANDLER() (void)0
# else
#  define OVERFLOW_HANDLER() abort()
# endif
#endif

#define OVERFLOW_DEFINE(OP, T)                  \
  static                                        \
  T OP ## _overflow_ ## T(T a, T b) {           \
    T ret;                                      \
    if (ckd_ ## OP(&ret, a, b)) {               \
      OVERFLOW_HANDLER();                       \
    }                                           \
    return ret;                                 \
  }                                             \
  static T OP ## _overflow_ ## T(T a, T b)

// define the 3 x 6 functions for the standard integer types
OVERFLOW_DEFINE(add, signed);
OVERFLOW_DEFINE(add, long);
OVERFLOW_DEFINE(add, llong);
OVERFLOW_DEFINE(add, unsigned);
OVERFLOW_DEFINE(add, ulong);
OVERFLOW_DEFINE(add, ullong);

OVERFLOW_DEFINE(sub, signed);
OVERFLOW_DEFINE(sub, long);
OVERFLOW_DEFINE(sub, llong);
OVERFLOW_DEFINE(sub, unsigned);
OVERFLOW_DEFINE(sub, ulong);
OVERFLOW_DEFINE(sub, ullong);

OVERFLOW_DEFINE(mul, signed);
OVERFLOW_DEFINE(mul, long);
OVERFLOW_DEFINE(mul, llong);
OVERFLOW_DEFINE(mul, unsigned);
OVERFLOW_DEFINE(mul, ulong);
OVERFLOW_DEFINE(mul, ullong);

// If uintmax_t is wider than ullong, it must be an extended type
// different from the standard types.
#if (ULLONG_WIDTH < UINTMAX_WIDTH)
OVERFLOW_DEFINE(add, intmax_t);
OVERFLOW_DEFINE(add, uintmax_t);
OVERFLOW_DEFINE(sub, intmax_t);
OVERFLOW_DEFINE(sub, uintmax_t);
OVERFLOW_DEFINE(mul, intmax_t);
OVERFLOW_DEFINE(mul, uintmax_t);
#define UINTmax_CASES(OP)                                       \
  intmax_t:    OP ## _overflow_intmax_t,                        \
    uintmax_t:   OP ## _overflow_uintmax_t, /* comma! */
#else
#define UINTMAX_CASES(OP) /* empty */
#endif

// If UINT128_WIDTH is greater than UINTMAX_WIDTH, it must be an
// extended type different from all types that we handled so far.
#if (UINTMAX_WIDTH < UINT128_WIDTH)
OVERFLOW_DEFINE(add, int128_t);
OVERFLOW_DEFINE(add, uint128_t);
OVERFLOW_DEFINE(sub, int128_t);
OVERFLOW_DEFINE(sub, uint128_t);
OVERFLOW_DEFINE(mul, int128_t);
OVERFLOW_DEFINE(mul, uint128_t);
#define UINT128_CASES(OP)                                       \
  int128_t:    OP ## _overflow_int128_t,                        \
    uint128_t:   OP ## _overflow_uint128_t, /* comma! */
#else
#define UINT128_CASES(OP) /* empty */
#endif

#define op_overflow(OP, A, B)                           \
  _Generic((A)+(B),                                     \
           UINTMAX_CASES(OP)                            \
           UINT128_CASES(OP)                            \
           signed:   OP ## _overflow_signed,            \
           long:     OP ## _overflow_long,              \
           llong:    OP ## _overflow_llong,             \
           unsigned: OP ## _overflow_unsigned,          \
           ulong:    OP ## _overflow_ulong,             \
           ullong:   OP ## _overflow_ullong)((A), (B))

#define add_overflow(A, B) op_overflow(add, (A), (B))
#define sub_overflow(A, B) op_overflow(sub, (A), (B))
#define mul_overflow(A, B) op_overflow(mul, (A), (B))

// Use a fence such that the assembler is not reordered too wildly
#define fence atomic_signal_fence(memory_order_seq_cst)


#ifndef TYPE
# define TYPE ullong
#endif

void startup0() {
  printf("starting 0\n");
}
void startup1() {
  printf("starting 1\n");
}

int main(int argc, char* argv[argc + 1]) {
  static once_flag start0 = ONCE_FLAG_INIT;
  static once_flag start1 = ONCE_FLAG_INIT;
  call_once(&start0, startup0);
  call_once(&start1, startup1);
  if (argc != 2) {
    fprintf(stderr, "we need exactly 1 arguments, found %d\n", argc-1);
  } else {
    TYPE a = strtoull(argv[1], nullptr, 0);
    TYPE result_add;
    TYPE result_sub;
    TYPE result_mul;

    for (TYPE b = 1; b < 4; ++b) {
      bool add_invalid = ckd_add(&result_add, a, b);
      fence;
      bool sub_invalid = ckd_sub(&result_sub, a, b);
      fence;
      bool mul_invalid = ckd_mul(&result_mul, a, b);
      fence;

      printf("operation add for %#llX and %#llX is %s, result is %#llX\n",
             (ullong)a, (ullong)b, (add_invalid ? "invalid" : "valid"), (ullong)result_add);
      printf("operation sub for %#llX and %#llX is %s, result is %#llX\n",
             (ullong)a, (ullong)b, (sub_invalid ? "invalid" : "valid"), (ullong)result_sub);
      printf("operation mul for %#llX and %#llX is %s, result is %#llX\n",
             (ullong)a, (ullong)b, (mul_invalid ? "invalid" : "valid"), (ullong)result_mul);
    }
    printf("operation add with overflow handling for %#llX and %#llX is %#llX\n",
           (ullong)a, (ullong)0x3, (ullong)add_overflow(a, 0x3));
    printf("operation sub with overflow handling for %#llX and %#llX is %#llX\n",
           (ullong)a, (ullong)0x3, (ullong)sub_overflow(a, 0x3));
    printf("operation mul with overflow handling for %#llX and %#llX is %#llX\n",
           (ullong)a, (ullong)0x3, (ullong)mul_overflow(a, 0x3));
  }
}
