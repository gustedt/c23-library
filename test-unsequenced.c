#include "c23-fallback.h"
#include <math.h>
#include <fenv.h>


double distance (double const x[static 2]);

double g (double y[restrict static 1], double const x[restrict static 2]) {
  // We assert that distance will not see different states of the floating
  // point environment.
  [[__unsequenced__]] extern double distance (double const x[static 2]);
  y[0] = distance(x);
  __asm__(";" : : );
  return distance(x);
  // replacement by y[0] is valid
}

[[__reproducible__]]
double distance (double const x[static 2]) {
#pragma FP_CONTRACT OFF
#pragma FENV_ROUND FE_TONEAREST
  // We assert that sqrt will not be called with invalid arguments
  // and the result only depends on the argument value.
  [[__unsequenced__]] extern __typeof__(sqrt) sqrt;
  return sqrt(x[0]*x[0] + x[1]*x[1]);
}

