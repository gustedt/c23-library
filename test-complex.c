// test for complex arithmetic without library inclusion

// Compiler versions know to work with this

// gcc-9
// clang-9

#include "c23-fallback.h"

int main(int argc, char* argv[argc+1]) {
  static _Complex float i = I;
  return i*i == -1.0F;
}
